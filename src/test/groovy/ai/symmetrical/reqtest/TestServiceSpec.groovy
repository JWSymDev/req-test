package ai.symmetrical.reqtest

import ai.symmetrical.reqtest.domain.services.TestService
import spock.lang.Specification

class TestServiceSpec extends Specification {

    TestService service = new TestService();

    def "Do something without failure"() {
        given:

        when:
        def result = service.calcSomething();

        then:
        noExceptionThrown()
        assert result == 1
    }

}


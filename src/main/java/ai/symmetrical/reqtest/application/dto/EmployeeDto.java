package ai.symmetrical.reqtest.application.dto;

import ai.symmetrical.reqtest.domain.models.Employee;
import java.util.UUID;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class EmployeeDto {

  UUID employeeId;

  String firstName;

  String lastName;

  public static EmployeeDto fromEmployee(Employee e) {
    return EmployeeDto.builder()
        .employeeId(e.getId())
        .firstName(e.getFirstName())
        .lastName(e.getLastName())
        .build();
  }
}

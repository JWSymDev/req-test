package ai.symmetrical.reqtest.application;

import ai.symmetrical.reqtest.domain.handlers.AddEmployeeHandler;
import ai.symmetrical.reqtest.domain.handlers.DeleteEmployeeHandler;
import ai.symmetrical.reqtest.domain.handlers.FindEmployeeHandler;
import ai.symmetrical.reqtest.domain.handlers.UpdateEmployeeHandler;
import ai.symmetrical.reqtest.domain.models.Employee;
import ai.symmetrical.reqtest.domain.models.commands.AddEmployee;
import ai.symmetrical.reqtest.domain.models.commands.DeleteEmployee;
import ai.symmetrical.reqtest.domain.models.commands.FindEmployee;
import ai.symmetrical.reqtest.domain.models.commands.UpdateEmployee;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmployeeFacade {

  private final AddEmployeeHandler addEmployeeHandler;
  private final FindEmployeeHandler findEmployeeHandler;
  private final UpdateEmployeeHandler updateEmployeeHandler;
  private final DeleteEmployeeHandler deleteEmployeeHandler;

  public Employee addEmployee(AddEmployee command) {
    return addEmployeeHandler.handle(command);
  }

  public Optional<Employee> findEmployee(FindEmployee query) {
    return findEmployeeHandler.handle(query);
  }

  public Employee updateEmployee(UpdateEmployee command) {
    return updateEmployeeHandler.handle(command);
  }

  public void deleteEmployee(DeleteEmployee command) {
    deleteEmployeeHandler.handle(command);
  }
}

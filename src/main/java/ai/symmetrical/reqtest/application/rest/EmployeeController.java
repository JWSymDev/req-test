package ai.symmetrical.reqtest.application.rest;

import ai.symmetrical.reqtest.application.EmployeeFacade;
import ai.symmetrical.reqtest.application.dto.EmployeeDto;
import ai.symmetrical.reqtest.domain.models.commands.AddEmployee;
import ai.symmetrical.reqtest.domain.models.commands.DeleteEmployee;
import ai.symmetrical.reqtest.domain.models.commands.FindEmployee;
import ai.symmetrical.reqtest.domain.models.commands.UpdateEmployee;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

@Slf4j
@RestController
@RequestMapping("/v1/employee")
@RequiredArgsConstructor
@Validated
public class EmployeeController {

  private final EmployeeFacade employeeFacade;

  @PostMapping
  ResponseEntity<?> createEmployee(@Validated @RequestBody AddEmployee addEmployee) {
    var employeeId = employeeFacade.addEmployee(addEmployee).getId();
    var location =
        MvcUriComponentsBuilder.fromMethodCall(
                MvcUriComponentsBuilder.on(this.getClass()).getEmployee(employeeId))
            .build()
            .toUri();
    return ResponseEntity.created(location).build();
  }

  @GetMapping("/{employeeId}")
  ResponseEntity<EmployeeDto> getEmployee(@PathVariable UUID employeeId) {
    var command = new FindEmployee(employeeId);
    var employee = employeeFacade.findEmployee(command);
    return employee
        .map(e -> ResponseEntity.ok(EmployeeDto.fromEmployee(e)))
        .orElse(ResponseEntity.notFound().build());
  }

  @PutMapping()
  ResponseEntity<EmployeeDto> updateEmployee(
      @Validated @RequestBody UpdateEmployee updateEmployee) {
    var updatedEmployee = employeeFacade.updateEmployee(updateEmployee);
    return ResponseEntity.ok(EmployeeDto.fromEmployee(updatedEmployee));
  }

  @DeleteMapping("/{employeeId}")
  ResponseEntity<?> deleteEmployee(@PathVariable UUID employeeId) {
    var command = new DeleteEmployee(employeeId);
    employeeFacade.deleteEmployee(command);
    return ResponseEntity.ok().build();
  }

}

package ai.symmetrical.reqtest.domain.repositories;

import ai.symmetrical.reqtest.domain.models.Employee;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, UUID> {

}

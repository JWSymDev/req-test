package ai.symmetrical.reqtest.domain.handlers;

import ai.symmetrical.reqtest.domain.models.Employee;
import ai.symmetrical.reqtest.domain.models.commands.AddEmployee;
import ai.symmetrical.reqtest.domain.repositories.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AddEmployeeHandler {

  private final EmployeeRepository repository;

  public Employee handle(AddEmployee command) {
    return repository.save(map(command));
  }

  private Employee map(AddEmployee command) {
    return Employee.builder()
        .firstName(command.getFirstName())
        .lastName(command.getLastName())
        .build();
  }
}

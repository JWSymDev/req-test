package ai.symmetrical.reqtest.domain.handlers;

import ai.symmetrical.reqtest.domain.models.Employee;
import ai.symmetrical.reqtest.domain.models.commands.FindEmployee;
import ai.symmetrical.reqtest.domain.repositories.EmployeeRepository;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FindEmployeeHandler {

  private final EmployeeRepository repository;

  public Optional<Employee> handle(FindEmployee query) {
    return repository.findById(query.getEmployeeId());
  }

}

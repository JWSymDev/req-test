package ai.symmetrical.reqtest.domain.handlers;

import ai.symmetrical.reqtest.domain.models.commands.DeleteEmployee;
import ai.symmetrical.reqtest.domain.repositories.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteEmployeeHandler {

  private final EmployeeRepository repository;

  public void handle(DeleteEmployee command) {
    var existingEmployee = repository.findById(command.getEmployeeId())
        .orElseThrow(() -> new IllegalArgumentException("Not existing Employee cannot be deleted!"));
    repository.delete(existingEmployee);
  }

}

package ai.symmetrical.reqtest.domain.handlers;

import ai.symmetrical.reqtest.domain.models.Employee;
import ai.symmetrical.reqtest.domain.models.commands.UpdateEmployee;
import ai.symmetrical.reqtest.domain.repositories.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateEmployeeHandler {

  private final EmployeeRepository repository;

  public Employee handle(UpdateEmployee command) {
    var existingEmployee = repository.findById(command.getEmployeeId())
        .orElseThrow(() -> new IllegalArgumentException("Not existing Employee cannot be updated!"));
    return repository.save(updateEmployee(command, existingEmployee));
  }

  private Employee updateEmployee(UpdateEmployee command, Employee existingEmployee) {
    return Employee.builder()
        .id(existingEmployee.getId())
        .firstName(command.getFirstName() != null ? command.getFirstName() : existingEmployee.getFirstName())
        .lastName(command.getLastName() != null ? command.getLastName() : existingEmployee.getLastName())
        .build();
  }

}

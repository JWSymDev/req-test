package ai.symmetrical.reqtest.domain.services;

import org.springframework.stereotype.Service;

@Service
public class TestService {

  public int calcSomething() {
    return 1;
  }
}

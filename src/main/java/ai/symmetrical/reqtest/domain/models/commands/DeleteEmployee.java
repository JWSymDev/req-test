package ai.symmetrical.reqtest.domain.models.commands;

import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.Value;

@Value
public class DeleteEmployee {

  @NotNull
  UUID employeeId;
}
package ai.symmetrical.reqtest.domain.models.commands;

import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Value;

@Value
public class UpdateEmployee {

  @NotNull
  UUID employeeId;

  @NotBlank
  String firstName;

  @NotBlank
  String lastName;
}
package ai.symmetrical.reqtest.domain.models.commands;

import javax.validation.constraints.NotBlank;
import lombok.Value;

@Value
public class AddEmployee {

  @NotBlank
  String firstName;

  @NotBlank
  String lastName;
}
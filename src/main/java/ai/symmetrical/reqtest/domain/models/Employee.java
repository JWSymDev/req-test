package ai.symmetrical.reqtest.domain.models;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "employee")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Employee {

  @Id
  @Builder.Default
  @Type(type = "uuid-char")
  @Column(updatable = false, nullable = false, unique = true)
  UUID id = UUID.randomUUID();

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;
}

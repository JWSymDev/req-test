CREATE TABLE IF NOT EXISTS employee
(
    id                                  VARCHAR(36) NOT NULL,
    first_name                          VARCHAR(36) NOT NULL,
    last_name                            VARCHAR(36) NOT NULL,
    PRIMARY KEY (id)
) ENGINE = INNODB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
